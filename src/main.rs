mod sds011_command;

use sds011_command::Sds011BaseCommand;

use std::io::{self, Read, Write};
use std::thread;
use std::time::Duration;

use std::sync::atomic::{AtomicBool, Ordering};
use std::sync::Arc;

use signal_hook::consts::signal::*;
use signal_hook::flag as signal_flag;

use clap::{Arg, Command};
use serialport::{self, SerialPort};

#[allow(dead_code)]
fn sensor_do_command(
    fd: &mut Box<dyn SerialPort>,
    command: Sds011BaseCommand,
) -> Option<Sds011BaseCommand> {
    let mut serial_buf: [u8; 10] = [0; 10];
    let towrite = Sds011BaseCommand::serialize(&command);
    println!("towrite = {:x?}", towrite);
    match fd.write(&towrite) {
        Ok(t) => {
            println!("Wrote {} number ob bytes.", t);
            match fd.read(&mut serial_buf) {
                Ok(t) => {
                    println!(
                        "Read answer ob version command {:x?}, len = {}",
                        serial_buf, t
                    );
                    return Sds011BaseCommand::deserialize(&serial_buf, serial_buf.len());
                }
                Err(ref e) if e.kind() == io::ErrorKind::TimedOut => (),
                Err(e) => eprintln!("{:?}", e),
            }
        }
        Err(ref e) if e.kind() == io::ErrorKind::TimedOut => (),
        Err(e) => eprintln!("{:?}", e),
    }

    None
}

fn sensor_read_status(fd: &mut Box<dyn SerialPort>) -> Option<Sds011BaseCommand> {
    let mut serial_buf: [u8; 10] = [0; 10];
    match fd.read(&mut serial_buf) {
        Ok(t) => {
            println!(
                "Read answer ob version command {:x?}, len = {}",
                serial_buf, t
            );
            return Sds011BaseCommand::deserialize(&serial_buf, serial_buf.len());
        }
        Err(ref e) if e.kind() == io::ErrorKind::TimedOut => (),
        Err(e) => eprintln!("{:?}", e),
    }

    None
}

fn main() {
    let matches = Command::new("PM Sensor SDS011 read")
        .about("Reads sensor data from a sds011 PM2.5 sensor")
        .disable_version_flag(true)
        .arg(
            Arg::new("port")
                .help("The device path to a serial port")
                .use_value_delimiter(false)
                .required(true),
        )
        .get_matches();

    let port_name = matches.get_one::<String>("port").unwrap();

    let term = Arc::new(AtomicBool::new(false));
    signal_flag::register(SIGINT, Arc::clone(&term)).unwrap();
    signal_flag::register(SIGTERM, Arc::clone(&term)).unwrap();
    signal_flag::register(SIGQUIT, Arc::clone(&term)).unwrap();

    println!("Serial port {}", port_name);

    let sds011_duty_cycle = Duration::from_millis(1000);

//    let version_command: Sds011BaseCommand = Sds011BaseCommand::builder()
//        .device_id(0xFFFF)
//        .cmd_dir(Sds011CommandDirection::Sds011DirSnd)
//        .cmd_type(Sds011CommandType::FirmwareVersion)
//        .build();

    let mut port = serialport::new(port_name, 9600)
        .path(port_name)
        .baud_rate(9600)
        .data_bits(serialport::DataBits::Eight)
        .stop_bits(serialport::StopBits::One)
        .parity(serialport::Parity::None)
        .flow_control(serialport::FlowControl::None)
        .timeout(Duration::from_millis(0))
        .open();

    let fd = match port {
        Ok(ref mut fd) => fd,
        Err(e) => {
            eprintln!("Failed to open \"{}\". Error: {}", port_name, e);
            ::std::process::exit(1);
        }
    };

    while !term.load(Ordering::Relaxed) {
        // sleep for the remainder of the duty cycle
        thread::sleep(sds011_duty_cycle);

        let result = match sensor_read_status(fd) {
            Some(x) => x,
            None => continue,
        };
        println!(
            "device({:#04x}) PM2.5: {} PM10: {}",
            result.get_device_id(),
            result.get_p25().unwrap(),
            result.get_p10().unwrap()
        );
    }
}
