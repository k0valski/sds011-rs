#![allow(dead_code)]

#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd)]
pub enum Sds011CommandDirection {
    Non = 0x00,
    Snd = 0xB4,
    Status = 0xC0,
    Rcv = 0xC5,
}

impl From<u8> for Sds011CommandDirection {
    fn from(x: u8) -> Self {
        match x {
            0xB4 => Sds011CommandDirection::Snd,
            0xC0 => Sds011CommandDirection::Status,
            0xC5 => Sds011CommandDirection::Rcv,
            _ => Sds011CommandDirection::Non,
        }
    }
}

#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd)]
pub enum Sds011CommandType {
    Non = 0x00,
    DataReport = 0x02,
    QueryData = 0x04,
    SetDevId = 0x05,
    SetWorkSleep = 0x06,
    DutyCycle = 0x08,
    FirmwareVersion = 0x07,
}

impl From<u8> for Sds011CommandType {
    fn from(x: u8) -> Self {
        match x {
            0x02 => Sds011CommandType::DataReport,
            0x04 => Sds011CommandType::QueryData,
            0x05 => Sds011CommandType::SetDevId,
            0x06 => Sds011CommandType::SetWorkSleep,
            0x07 => Sds011CommandType::FirmwareVersion,
            0x08 => Sds011CommandType::DutyCycle,
            _ => Sds011CommandType::Non,
        }
    }
}

fn calculate_checksum(data: &[u8]) -> u8 {
    let mut res: u16 = 0;
    for x in data {
        res += *x as u16;
    }

    (res & 0x00FF) as u8
}

// Receive: 6 data bytes, 1 head byte, 1 tail byte, 1 cmd id byte, 1 checksum byte
// Send: 15 data bytes, 1 head byte, 1 tail byte, 1 cmd id byte, 1 checksum byte
#[derive(Copy, Clone, PartialEq, Eq, Ord, PartialOrd)]
pub struct Sds011BaseCommand {
    cmd_dir: Sds011CommandDirection,
    cmd_type: Sds011CommandType,
    device_id: u16,
    data: [u8; 15],
}

impl Sds011BaseCommand {
    pub fn new() -> Sds011BaseCommand {
        Sds011BaseCommand {
            cmd_dir: (Sds011CommandDirection::Non),
            cmd_type: (Sds011CommandType::Non),
            device_id: (0x397E),
            data: ([0; 15]),
        }
    }

    pub fn builder() -> Sds011BaseCommandBuilder {
        Sds011BaseCommandBuilder {
            cmd_dir: Sds011CommandDirection::Non,
            cmd_type: Sds011CommandType::Non,
            device_id: 0,
        }
    }

    pub fn deserialize(buffer: &[u8], buffer_len: usize) -> Option<Sds011BaseCommand> {
        let mut message = Sds011BaseCommand::new();

        if buffer_len > 19 {
            return None;
        }

        if buffer[0] != 0xAA || buffer[buffer_len - 1] != 0xAB {
            return None;
        }

        message.set_device_id(
            ((buffer[buffer_len - 4] as u16) << 8) + (buffer[buffer_len - 3] as u16),
        );

        message.set_cmd_dir(buffer[1].into());
        if message.get_cmd_dir() == Sds011CommandDirection::Non {
            println!("get_cmd_dir = {:#02x}", message.get_cmd_dir() as u8);
            return None;
        }

        if message.get_cmd_dir() == Sds011CommandDirection::Status {
            message.set_cmd_type(Sds011CommandType::QueryData);
        } else {
            message.set_cmd_type(buffer[2].into());
            if message.get_cmd_type() == Sds011CommandType::Non {
                println!("get_cmd_type = {:#02x}", message.get_cmd_type() as u8);
                return None;
            }
        }

//        for x in 0..buffer[2..8].to_vec().len() {
//            message.data[x] = buffer[x + 2];
//        }
        message.data.copy_from_slice(&buffer[2..8]);

        let msg_chksm = calculate_checksum(&message.data);
        if msg_chksm != buffer[buffer_len - 2] {
            println!(
                "caculated checksum {:#02x} does not match message chksm {:02x}",
                msg_chksm,
                buffer[buffer_len - 2]
            );
            return None;
        }

        Some(message)
    }

    pub fn serialize(&self) -> [u8; 19] {
        let mut res: [u8; 19] = [0; 19];

//        for i in 0..self.data.len() {
//            res[i + 2] = self.data[i];
//        }
        res[2..(self.data.len() + 2)].copy_from_slice(&self.data[..]);

        res[0] = 0xAA;
        res[1] = self.cmd_dir as u8;
        res[2] = self.cmd_type as u8;

        if self.cmd_dir == Sds011CommandDirection::Snd {
            res[15] = (self.device_id & 0x00FF) as u8;
            res[16] = ((self.device_id >> 8) & 0x00FF) as u8;
            res[17] = calculate_checksum(&res[2..17]);
            res[18] = 0xAB;
        } else if self.cmd_dir == Sds011CommandDirection::Rcv {
            res[6] = (self.device_id & 0x00FF) as u8;
            res[7] = ((self.device_id >> 8) & 0x00FF) as u8;
            res[8] = calculate_checksum(&res[2..8]);
            res[9] = 0xAB;
        } else {
            panic!("Invalid direction");
        }

        res
    }

    pub fn get_device_id(&self) -> u16 {
        self.device_id
    }

    pub fn set_device_id(&mut self, device_id: u16) {
        self.device_id = device_id;
    }

    pub fn set_cmd_dir(&mut self, cmd_dir: Sds011CommandDirection) {
        self.cmd_dir = cmd_dir;
    }

    pub fn get_cmd_dir(&self) -> Sds011CommandDirection {
        self.cmd_dir
    }

    pub fn set_cmd_type(&mut self, cmd_type: Sds011CommandType) {
        self.cmd_type = cmd_type;
    }

    pub fn get_cmd_type(&self) -> Sds011CommandType {
        self.cmd_type
    }

    //	0x00, // data byte 2 (Year)
    //	0x00, // data byte 3 (Month)
    //	0x00, // data byte 4 (Day)
    //	only firmware command version
    pub fn get_version(&self) -> (u8, u8, u8) {
        if self.data[0] != Sds011CommandType::FirmwareVersion as u8 {
            return (0, 0, 0);
        }

        (self.data[1], self.data[2], self.data[3])
    }

    // QueryData BEGIN
    // SEND
    //	0x04, // data byte 1
    //	0x00, // data byte 2
    //	0x00, // data byte 3
    //	0x00, // data byte 4
    //	0x00, // data byte 5
    //	0x00, // data byte 6
    //	0x00, // data byte 7
    //	0x00, // data byte 8
    //	0x00, // data byte 9
    //	0x00, // data byte 10
    //	0x00, // data byte 11
    //	0x00, // data byte 12
    //	0x00, // data byte 13
    //	0xFF, // data byte 14 (device id byte 1)
    //	0xFF, // data byte 15 (device id byte 2)
    // REPLY
    //	0x00, // data byte 1 (PM2.5 low byte)
    //	0x00, // data byte 2 (PM2.5 high byte)
    //	0x00, // data byte 3 (PM10 low byte)
    //	0x00, // data byte 4 (PM10 high byte)
    //	0x00, // data byte 5 (device id byte 1)
    //	0x00, // data byte 6 (device id byte 2)
    pub fn get_p25(&self) -> Option<f32> {
        if self.cmd_type != Sds011CommandType::QueryData {
            return None;
        }
        let mut result = self.data[0] as f32;
        result += (self.data[1] as f32) * 256.0;
        result /= 10.0;
        Some(result)
    }

    pub fn get_p10(&self) -> Option<f32> {
        if self.cmd_type != Sds011CommandType::QueryData {
            return None;
        }
        let mut result = self.data[2] as f32;
        result += (self.data[3] as f32) * 256.0;
        result /= 10.0;
        Some(result)
    }
    // QueryData END

    // DutyCycle BEGIN
    pub fn get_duty_cycle_command_mode(&self) -> Option<u8> {
        if self.cmd_type != Sds011CommandType::DutyCycle {
            return None;
        }
        Some(self.data[1])
    }

    pub fn get_duty_cycle(&self) -> Option<u8> {
        if self.cmd_type != Sds011CommandType::DutyCycle {
            return None;
        }
        Some(self.data[2])
    }

    pub fn set_duty_cycle_command_mode(&mut self, duty_cycle_command_mode: u8) {
        if self.cmd_type != Sds011CommandType::DutyCycle {
            return;
        }
        self.data[1] = duty_cycle_command_mode;
    }

    pub fn set_duty_cycle(&mut self, duty_cycle: u8) {
        if self.cmd_type != Sds011CommandType::DutyCycle {
            return;
        }
        self.data[2] = duty_cycle;
    }
    // DutyCycle END

    // SetDevId BEGIN
    // SEND
    //	0x05, // data byte 1
    //	0x00, // data byte 2
    //	0x00, // data byte 3
    //	0x00, // data byte 4
    //	0x00, // data byte 5
    //	0x00, // data byte 6
    //	0x00, // data byte 7
    //	0x00, // data byte 8
    //	0x00, // data byte 9
    //	0x00, // data byte 10
    //	0x00, // data byte 11
    //	0x00, // data byte 12 (new device id byte 1)
    //	0x00, // data byte 13 (new device id byte 2)
    //	0xFF, // data byte 14 (device id byte 1)
    //	0xFF, // data byte 15 (device id byte 2)
    // REPLY
    //	0x05, // data byte 1
    //	0x00, // data byte 2
    //	0x00, // data byte 3
    //	0x00, // data byte 4
    //	0x00, // data byte 5 (new device id byte 1)
    //	0x00, // data byte 6 (new device id byte 2)

    // SetDevId END
    //
    // DataReport BEGIN
    // SEND
    //	0x02, // data byte 1
    //	0x01, // data byte 2 (0：query the current mode 1：set reporting mode)
    //	0x00, // data byte 3 (0：report active mode 1：Report query mode)
    //	0x00, // data byte 4
    //	0x00, // data byte 5
    //	0x00, // data byte 6
    //	0x00, // data byte 7
    //	0x00, // data byte 8
    //	0x00, // data byte 9
    //	0x00, // data byte 10
    //	0x00, // data byte 11
    //	0x00, // data byte 12
    //	0x00, // data byte 13
    //	0xFF, // data byte 14 (device id byte 1)
    //	0xFF, // data byte 15 (device id byte 2)
    // REPLY
    //	0x02, // data byte 1
    //	0x01, // data byte 2 (0：query the current mode 1：set reporting mode)
    //	0x00, // data byte 3 (0：report active mode 1：Report query mode)
    //	0x00, // data byte 4
    //	0x00, // data byte 5 (device id byte 1)
    //	0x00, // data byte 6 (device id byte 2)

    pub fn set_data_reporting_mode(&mut self, reporting_mode: u8) {
        if self.cmd_type != Sds011CommandType::DataReport {
            return;
        }
        self.data[1] = reporting_mode;
    }

    pub fn set_reporting_mode_query(&mut self, reporting_mode_query: u8) {
        if self.cmd_type != Sds011CommandType::DataReport {
            return;
        }
        self.data[1] = 1;
        self.data[2] = reporting_mode_query;
    }

    pub fn get_data_reporting_query_mode(&self) -> Option<u8> {
        if self.cmd_type != Sds011CommandType::DataReport {
            return None;
        }

        Some(self.data[1])
    }

    pub fn get_data_reporting_mode(&self) -> Option<u8> {
        if self.cmd_type != Sds011CommandType::DataReport {
            return None;
        }

        Some(self.data[2])
    }
    // DataReport BEGIN

    // Sds011CommandWorkingMode BEGIN
    // SEND
    //	0x06, // data byte 1
    //	0x01, // data byte 2 (0：query the current mode 1：set reporting mode)
    //	0x00, // data byte 3 (0: sleep 1: work)
    //	0x00, // data byte 4
    //	0x00, // data byte 5
    //	0x00, // data byte 6
    //	0x00, // data byte 7
    //	0x00, // data byte 8
    //	0x00, // data byte 9
    //	0x00, // data byte 10
    //	0x00, // data byte 11
    //	0x00, // data byte 12
    //	0x00, // data byte 13
    //	0xFF, // data byte 14 (device id byte 1)
    //	0xFF, // data byte 15 (device id byte 2)
    //	0x00, // checksum
    //	0xAB  // tail
    // REPLY
    //	0x06, // data byte 1
    //	0x00, // data byte 2 (0：query the current mode 1：set reporting mode)
    //	0x00, // data byte 3 (0: sleep 1: work)
    //	0x00, // data byte 4
    //	0x00, // data byte 5 (device id byte 1)
    //	0x00, // data byte 6 (device id byte 2)
    pub fn set_sleep_work_mode_query(&mut self, query_mode: u8) {
        if self.cmd_type != Sds011CommandType::SetWorkSleep {
            return;
        }
        self.data[1] = query_mode;
    }

    pub fn set_sleep_work_mode(&mut self, sleep_work_mode: u8) {
        if self.cmd_type != Sds011CommandType::SetWorkSleep {
            return;
        }
        self.data[1] = 1;
        self.data[2] = sleep_work_mode;
    }

    pub fn get_sleep_work_mode_query(&self) -> Option<u8> {
        if self.cmd_type != Sds011CommandType::SetWorkSleep {
            return None;
        }
        Some(self.data[1])
    }

    pub fn get_sleep_work_mode(&self) -> Option<u8> {
        if self.cmd_type != Sds011CommandType::SetWorkSleep {
            return None;
        }
        Some(self.data[2])
    }
    // Sds011CommandWorkingMode END
}

pub struct Sds011BaseCommandBuilder {
    cmd_dir: Sds011CommandDirection,
    cmd_type: Sds011CommandType,
    device_id: u16,
}

impl Sds011BaseCommandBuilder {
    pub fn cmd_dir(mut self, new_cmd_dir: Sds011CommandDirection) -> Self {
        self.cmd_dir = new_cmd_dir;
        self
    }

    pub fn cmd_type(mut self, new_cmd_type: Sds011CommandType) -> Self {
        self.cmd_type = new_cmd_type;
        self
    }

    pub fn device_id(mut self, new_device_id: u16) -> Self {
        self.device_id = new_device_id;
        self
    }

    pub fn build(self) -> Sds011BaseCommand {
        Sds011BaseCommand {
            cmd_dir: (self.cmd_dir),
            cmd_type: (self.cmd_type),
            device_id: (self.device_id),
            data: ([0; 15]),
        }
    }
}
